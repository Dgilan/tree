angular.module('mk.tree').factory('mkBuilder', ['$compile', function ($compile) {
    'use strict';

    var uuid = function () {
            function _p8(s) {
                var p = (Math.random().toString(16) + '000000000').substr(2, 8);
                return s ? '-' + p.substr(0, 4) + '-' + p.substr(4, 4) : p;
            }

            return _p8() + _p8(true) + _p8(true) + _p8();
        },
        convertToNode = function (data, children, options, root) {
            //TODO: set data.label as dynamic key
            var hasChildren = typeof data.hasChildren === 'undefined' ? children.length > 0 : data.hasChildren;
            return {
                label: data.label ? data.label : 'No label',
                hasChildren: hasChildren,
                branch: data,
                children: children,
                collapsed: true,
                selected: false,
                editable: false,
                receivedChildren: hasChildren && options.ajax,
                icon: hasChildren ? options.classes.iconExpand : options.classes.iconLeaf,
                uuid: uuid(),
                rootElement: root
            };
        },
        buildTree = function (treeData, options, setAsRoot) {
            var tree = [], isRoot = typeof setAsRoot !== 'undefined' ? setAsRoot : true;

            function doBuild(node) {
                var children = [];
                angular.forEach(node.children, function (child) {
                    children.push(doBuild(child));
                });
                return convertToNode(node, children, options);
            }

            angular.forEach(treeData, function (node) {
                var n = doBuild(node);
                if (isRoot) {
                    n.rootElement = true;
                }
                tree.push(n);
            });
            return tree;
        };

    return {
        uuid: uuid,

        /**
         * Manually compiles the element, fixing the recursion loop.
         * @param {Node} element
         * @param {Function|Object} [link] A post-link function, or an object with function(s) registered via pre and post properties.
         * @returns {Object} An object containing the linking functions.
         */
        compile: function (element, link) {
            // Normalize the link parameter
            if (angular.isFunction(link)) {
                link = { post: link };
            }

            // Break the recursion loop by removing the contents
            var contents = element.contents().remove(),
                compiledContents;

            return {
                pre: (link && link.pre) ? link.pre : null,
                /**
                 * Compiles and re-adds the contents
                 */
                post: function (scope, element) {
                    // Compile the contents
                    if (!compiledContents) {
                        compiledContents = $compile(contents);
                    }
                    // Re-add the compiled contents to the element
                    compiledContents(scope, function (clone) {
                        element.append(clone);
                    });

                    // Call the post-linking function, if any
                    if (link && link.post) {
                        link.post.apply(null, arguments);
                    }
                }
            };
        },
        convertToNode: convertToNode,
        buildTree: buildTree,
        newNode: function (options) {
            return {
                label: 'New node',
                hasChildren: false,
                branch: undefined,
                children: [],
                collapsed: false,
                selected: false,
                editable: true,
                receivedChildren: false,
                icon: options.classes.iconLeaf,
                uuid: uuid(),
                rootElement: null
            };
        },
        findById: function (tree, id) {
            var doFind = function (node, parents) {
                var res;
                parents.push(node);
                angular.forEach(node.children, function (child) {
                    res = res || ((id.indexOf(child.branch.id) > -1) ? {
                        node: child,
                        parents: parents
                    } : doFind(child, parents));
                });
                return res;
            }, result = {'node': [], 'parents': []}, data;

            if(!angular.isArray(id)){
                id = [id];
            }

            angular.forEach(tree, function (node) {
                data = ((id.indexOf(node.branch.id) > -1) ? {'node': node, 'parents': []} : doFind(node, []));
                if(data) {
                    result.node.push(data.node);
                    result.parents = result.parents.concat(data.parents);
                }
            });
            return result;
        }
    };
}]);

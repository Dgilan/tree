angular.module('mk.tree', []).constant('mkTreeConfig', {
    classes: {
        over: 'mk-droppable-over',
        target: 'mk-droppable-target',
        intended: 'mk-intended',
        iconLeaf: 'glyphicon glyphicon-unchecked',
        iconExpand: 'glyphicon glyphicon-chevron-right',
        iconCollapse: 'glyphicon glyphicon-chevron-down',
        iconAdd: 'glyphicon glyphicon-plus-sign',
        iconRemove: 'glyphicon glyphicon-remove-sign',
        iconDraggable: 'glyphicon glyphicon-move',
        editableBtnGroup: 'mk-btn-group',
        selected: 'mk-selected',
        li: 'mk-tree-row',
        draggable: 'mk-tree-draggable'
    },
    translations: {
        confirmRemove: 'Are you sure you want to remove the item?'
    },
    ajax: false,
    editable: false,
    showRoot: false,
    selectOnlyChildren: false,
    expandOnSelect: true,
    multipleSelect: false
});

/**
 * Data provider mechanism for jasmine
 * @param {String} name Test case name
 * @param {Array} values Arguments
 * @param {Function} func Function to be called
 */
function using(name, values, func) {
    'use strict';
    for (var i = 0, count = values.length; i < count; i++) {
        if (Object.prototype.toString.call(values[i]) !== '[object Array]') {
            values[i] = [values[i]];
        }
        func.apply(this, values[i]);
        jasmine.currentEnv_.currentSpec.description += ' (with "' + name + '" using ' + values[i].join(', ') + ')';
    }
}
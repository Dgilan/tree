describe('Directive: mkTreeEdit', function () {
    'use strict';

    var runTest = function (html, run) {
        inject(function ($rootScope, $compile, $timeout) {
            var scope = $rootScope.$new(),
                el, input, link;

            scope.node = {
                label: 'Editable Node',
                editable: false
            };

            scope.save = function () {
            };

            el = $compile(html)(scope);

            scope.$apply();

            link = el.find('a');
            input = el.find('input');

            $(document).find('body').append(el);

            run(el, input, link, scope, $timeout);

            el.remove();
            scope.$destroy();
        });
    };

    beforeEach(module('mk.tree'));

    it('linking the directive', function () {
        var html = '<mk-tree-edit model="node"></mk-tree-edit>';
        spyOn($.fn, 'bind').andCallThrough();
        spyOn($.fn, 'clickoutside').andCallThrough();

        runTest(html, function (el, input, link) {
            expect($.fn.clickoutside.calls.length).toBe(1);
            expect($.fn.bind.calls.length).toBe(2);
            expect(link.hasClass('ng-hide')).toBeFalsy();
            expect(input.hasClass('ng-hide')).toBeTruthy();
            expect(link.text()).toBe('Editable Node');
            expect(input.val()).toBe('Editable Node');
        });
    });

    it('setting editable mode', function () {
        var html = '<mk-tree-edit model="node"></mk-tree-edit>';
        runTest(html, function (el, input, link, scope, timeout) {
            scope.node.editable = true;
            //scope.$digest();
            timeout(function () {
                expect(link.hasClass('ng-hide')).toBeTruthy();
                expect(input.hasClass('ng-hide')).toBeFalsy();
                expect(input.is(':focus')).toBeTruthy();
            });
        });
    });

    it('escaping editing in the input', function () {
        var html = '<mk-tree-edit model="node"></mk-tree-edit>';

        runTest(html, function (el, input, link, scope, timeout) {
            var event = new $.Event('keydown');
            scope.node.editable = true;
            scope.node.label = 'New text';
            timeout(function () {
                expect(input.val()).toBe('New text');
                event.keyCode = 27; //Escape
                input.trigger(event);
                expect(link.hasClass('ng-hide')).toBeFalsy();
                expect(input.hasClass('ng-hide')).toBeTruthy();
                expect(input.val()).toBe('Editable Node');
            });
        });
    });

    it('escaping editing in the window', function () {
        var html = '<mk-tree-edit model="node"></mk-tree-edit>';
        runTest(html, function (el, input, link, scope, timeout) {
            var event = new $.Event('keydown');
            scope.node.editable = true;
            scope.node.label = 'New text';
            timeout(function () {
                expect(input.val()).toBe('New text');

                event.keyCode = 27; //Escape
                $(document).trigger(event);

                expect(link.hasClass('ng-hide')).toBeFalsy();
                expect(input.hasClass('ng-hide')).toBeTruthy();
                expect(input.val()).toBe('Editable Node');
            });
        });
    });

    it('entering into the editing mode but not changing the value', function () {
        var html = '<mk-tree-edit model="node" on-save="save(node)"></mk-tree-edit>';
        runTest(html, function (el, input, link, scope) {
            var event = new $.Event('keydown');

            spyOn(scope, 'save');

            scope.node.editable = true;

            event.keyCode = 13; // Enter
            input.trigger(event);

            expect(scope.save.callCount).toBe(0);
            expect(scope.node.label).toBe('Editable Node');
        });
    });

    it('entering into the editing mode and changing the value', function () {
        var html = '<mk-tree-edit model="node" on-save="save(node)"></mk-tree-edit>';
        runTest(html, function (el, input, link, scope) {
            var event = new $.Event('keydown');
            scope.node.editable = true;
            scope.node.label = 'New text';

            spyOn(scope, 'save');
            event.keyCode = 13; // Enter
            input.trigger(event);

            expect(scope.save).toHaveBeenCalledWith(scope.node);
            expect(scope.node.label).toBe('New text');
            expect(scope.node.editable).toBeFalsy();
        });
    });

});
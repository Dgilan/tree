describe('Directive: mkDroppable', function () {
    'use strict';

    var scope, $compile, rootScope, mkTreeConfig, originalEvent = {
        dataTransfer: {
            data: {},
            dropEffect: null,
            setData: function (key, data) {
                this.data[key] = data;
            },
            getData: function (key) {
                return this.data[key];
            }
        }
    };

    beforeEach(module('mk.tree', function ($provide) {
        $provide.decorator('mkBuilder', function ($delegate) {
            $delegate.uuid = function () {
                return '1111-1111-1111';
            };
            return $delegate;
        });
    }));

    beforeEach(inject(function (_$rootScope_, _$compile_, _mkTreeConfig_) {
        scope = _$rootScope_.$new();
        scope.onDrop = function () {

        };
        rootScope = _$rootScope_;
        $compile = _$compile_;
        mkTreeConfig = _mkTreeConfig_;
    }));

    it('linking the directive', function () {
        spyOn($.fn, 'bind').andCallThrough();
        spyOn(rootScope, '$on').andCallThrough();

        var el = $compile('<div mk-droppable></div>')(scope);
        expect($.fn.bind.calls.length).toBe(3);
        expect(rootScope.$on.calls.length).toBe(3);
        expect(el.attr('id')).toBe('1111-1111-1111');
    });

    it('starts dragging', function () {
        var el = $compile('<div mk-droppable id="dropEl"></div>')(scope);

        rootScope.$emit('MK-DRAG-START');
        expect(el.hasClass(mkTreeConfig.classes.target)).toBe(true);
    });

    it('entering to the drop zone', function () {
        var el = $compile('<div mk-droppable id="dropEl"></div>')(scope);

        spyOn(rootScope, '$emit').andCallThrough();

        el.trigger('dragenter');
        expect(el.hasClass(mkTreeConfig.classes.over)).toBe(true);
        expect(rootScope.$emit).toHaveBeenCalledWith('MK-DRAG-ENTER', 'dropEl');
    });

    it('setting drop effect', function () {
        var el = $compile('<div mk-droppable id="dropEl"></div>')(scope),
            event = new $.Event('dragover', {originalEvent: originalEvent});

        el.trigger(event);
        expect(originalEvent.dataTransfer.dropEffect).toBe('move');
    });

    it('stopping dragging', function () {
        var el = $compile('<div mk-droppable id="dropEl"></div>')(scope);

        rootScope.$emit('MK-DRAG-START');
        el.trigger('dragenter');

        rootScope.$emit('MK-DRAG-END');
        expect(el.hasClass(mkTreeConfig.classes.target)).toBeFalsy();
        expect(el.hasClass(mkTreeConfig.classes.over)).toBeFalsy();
    });

    it('dropping on the target element', function () {
        var dest = $compile('<div mk-droppable id="dropEl" on-drop="onDrop(dragEl, dropEl)"></div>')(scope),
            src = $compile('<div id="dragEl"></div>')(scope),
            event = new $.Event('drop', {originalEvent: originalEvent});

        $(document).find('body').append(src);
        $(document).find('body').append(dest);

        spyOn(scope, 'onDrop').andCallThrough();

        originalEvent.dataTransfer.setData('text', 'dragEl');
        dest.trigger(event);

        expect(scope.onDrop.calls.length).toBe(1);
        expect(scope.onDrop).toHaveBeenCalledWith(src.scope(), dest.scope());
    });

});
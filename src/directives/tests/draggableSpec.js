describe('Directive: mkDraggable', function () {
  'use strict';

  var scope, $compile, rootScope,
    originalEvent = {
      dataTransfer: {
        data: {},
        setData: function (key, data) {
          this.data[key] = data;
        },
        getData: function (key) {
          return this.data[key];
        }
      }
    };

  beforeEach(module('mk.tree', function ($provide) {
    $provide.decorator('mkBuilder', function ($delegate) {
      $delegate.uuid = function () {
        return '1111-1111-1111';
      };
      return $delegate;
    });
  }));

  beforeEach(inject(function (_$rootScope_, _$compile_) {
    scope = _$rootScope_.$new();
    rootScope = _$rootScope_;
    $compile = _$compile_;
  }));

  it('linking the directive', function () {
    spyOn($.fn, 'bind').andCallThrough();
    var el = $compile('<div mk-draggable></div>')(scope);
    expect($.fn.bind.calls.length).toBe(2);
    expect(el.attr('id')).toBe('1111-1111-1111');
    expect(el.attr('draggable')).toBe('true');
  });

  it('linking the directive with id', function () {
    var el = $compile('<div id="2222" mk-draggable></div>')(scope);
    expect(el.attr('id')).toBe('2222');
  });

  it('element starts dragging', function () {
    var el = $compile('<div mk-draggable id="dragEl"></div>')(scope),
      event;
    spyOn(rootScope, '$emit');

    event = new $.Event('dragstart', {originalEvent: originalEvent});

    el.trigger(event);

    expect(originalEvent.dataTransfer.getData('text')).toBe('dragEl');
    expect(rootScope.$emit).toHaveBeenCalledWith('MK-DRAG-START');
  });

  it('element stops dragging', function () {
    var el = $compile('<div mk-draggable></div>')(scope);
    spyOn(rootScope, '$emit');
    el.trigger('dragend');

    expect(rootScope.$emit).toHaveBeenCalledWith('MK-DRAG-END');
  });

});
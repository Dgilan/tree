describe('Directive: mkTreeChildren', function () {
    'use strict';
    var scope, initData, options,
        html = '<mk-tree-children data="tree" options="options"></mk-tree-children>',
        runTest = function (html, data, run, opt) {
            inject(function ($rootScope, $compile, $timeout) {
                scope = $rootScope.$new();

                var el = $compile(html)(scope);
                $(document).find('body').append(el);

                scope.tree = data;
                scope.options = angular.extend(options, opt);
                scope.$apply();

                run(el, $timeout);

                el.remove();
                scope.$destroy();
            });
        },
        findModel = function (tree, id) {

            var doFind = function (node) {
                var res;
                angular.forEach(node.children, function (child) {
                    res = res || (id === child.uuid ? child : doFind(child));
                });

                return res;
            }, result;

            angular.forEach(tree, function (node) {
                result = result || (id === node.uuid ? node : doFind(node));
            });

            return result;
        };

    beforeEach(module('mk.tree'));

    beforeEach(inject(function () {
        options = {
            classes: {
                'iconExpand': 'fa fa-lg fa-plus-circle',
                'iconCollapse': 'fa fa-lg fa-plus-minus',
                'iconLeaf': 'fa fa-lg fa-leaf',
                'iconAdd': 'fa fa-plus fa-lg',
                'iconEdit': 'fa fa-edit fa-lg',
                'iconComment': 'fa fa-comment-o fa-lg',
                'iconRemove': 'fa fa-times-circle-o fa-lg',
                'iconDraggable': 'fa fa-lg fa-ellipsis-vertical',
                'iconClosed': 'fa fa-lg fa-minus-square',
                'iconOpened': 'fa fa-lg fa-check-square ',
                'editableBtnGroup': 'pull-right mk-btn-group'
            },

            'confirmHeader': 'Please, confirm!',
            'confirmRemove': 'Are you sure you want to remove the node?',
            'eventOnDblClick': false,
            'rootActions': false,
            'liClass': '',
            'expandOnSelect': false,

            'ajax': false,
            'editable': false,
            'showRoot': false,
            'selectOnlyChildren': false
        };
        initData = [
            {
                label: 'Root Node',
                hasChildren: true,
                branch: {
                    label: 'Root Node',
                    children: [
                        {
                            label: 'Node1',
                            children: [],
                            hasChildren: false,
                            id: 'node-1'
                        }
                    ],
                    hasChildren: true,
                    id: 'root-node'
                },
                children: [
                    {
                        label: 'Node1',
                        hasChildren: false,
                        branch: {
                            label: 'Node1',
                            children: [],
                            hasChildren: false,
                            id: 'node-1'
                        },
                        children: [],
                        collapsed: true,
                        selected: false,
                        editable: false,
                        receivedChildren: false,
                        icon: options.classes.iconLeaf,
                        uuid: 'uuid-node-1',
                        rootElement: null
                    }
                ],
                collapsed: true,
                selected: false,
                editable: false,
                receivedChildren: true,
                icon: options.classes.iconExpand,
                uuid: 'uuid-root-node',
                rootElement: null
            }
        ];
    }));

    describe('expanding', function () {

        it('toggling element', function () {
            runTest(html, initData, function (el) {
                var btnToggle = el.find('li > span >i');

                //initial state is collapsed
                expect(scope.tree[0].collapsed).toBeTruthy();
                expect(btnToggle.hasClass('fa-plus-circle')).toBeTruthy();

                //expand tree
                btnToggle.trigger('click');
//                scope.$digest();
                expect(btnToggle.hasClass('fa-plus-minus')).toBeTruthy();
                expect(scope.tree[0].collapsed).toBeFalsy();

                //collapse tree
                btnToggle.trigger('click');
                //scope.$digest();
                expect(scope.tree[0].collapsed).toBeTruthy();
                expect(btnToggle.hasClass('fa-plus-circle')).toBeTruthy();
            });
        });

        it('expanding tree when ajax is enabled but node already has children', function () {
            options.ajax = true;
            runTest(html, initData, function (el) {
                var btnToggle = el.find('li > span >i');
                spyOn(scope, '$emit').andCallThrough();
                //expand tree
                btnToggle.trigger('click');
                //scope.$digest();
                expect(btnToggle.hasClass('fa-plus-minus')).toBeTruthy();
                expect(scope.tree[0].collapsed).toBeFalsy();
                expect(scope.$emit).not.toHaveBeenCalled();//because of node.receivedChildren=true
            }, {ajax: true});
        });

        it('expanding tree when ajax is enabled and node receivedChildren is false', function () {
            initData[0].receivedChildren = false;
            initData[0].children = [];
            runTest(html, initData, function (el) {
                var btnToggle = el.find('li > span >i'),
                    response = {data: {data: [
                        {
                            label: 'Node2',
                            hasChildren: false,
                            children: [],
                            id: 'node-2'
                        }
                    ]}};

                spyOn(scope, '$emit').andCallThrough();

                //expand tree
                btnToggle.trigger('click');
                //scope.$digest();
                expect(btnToggle.hasClass('fa-plus-minus')).toBeTruthy();
                expect(scope.tree[0].collapsed).toBeFalsy();
                expect(scope.$emit).toHaveBeenCalledWith('MK-TREE-EXPAND', jasmine.any(Object), jasmine.any(Function));

                //Since 3th parameter is a callback for processing AJAX request that returns node children, lets test that received
                //data has been added to the tree
                scope.$emit.calls[0].args[2](response);
                expect(scope.tree[0].children.length).toBe(1);
                expect(scope.tree[0].receivedChildren).toBeTruthy();
                expect(scope.tree[0].children[0].label).toBe('Node2');
            }, {ajax: true});


        });
    });

    describe('selecting', function () {
        it('selecting tree', function () {
            var data2 = angular.copy(initData);

            data2[0].hasChildren = false;

            using('selecting node when tree options allow it', [
                {data: initData, opts: {selectOnlyChildren: false}},
                {data: data2, opts: {selectOnlyChildren: true}}
            ], function (argument) {

                runTest(html, argument.data, function (el) {
                    var span = el.find('li > span > a');

                    spyOn(scope, '$emit');

                    span.trigger('click');
                    //scope.$digest();

                    expect(scope.tree[0].selected).toBeTruthy();
                    expect(scope.$emit).toHaveBeenCalledWith('MK-TREE-SELECT', scope.tree[0]);
                }, argument.opts);
            });
        });

        it('selecting node is not allowed', function () {
            runTest(html, initData, function (el) {
                var span = el.find('li > span > a');
                spyOn(scope, '$emit');
                span.trigger('click');
                //scope.$digest();
                expect(scope.tree[0].selected).toBeFalsy();
                expect(scope.$emit).not.toHaveBeenCalled();
            }, {selectOnlyChildren: true})
        });

        it('expanding during selecting node', function () {
            initData[0].children = [];
            initData[0].receivedChildren = false;
            runTest(html, initData, function (el) {
                var span = el.find('li > span > a'),
                    spy = spyOn(scope, '$emit');
                span.trigger('click');
                //scope.$digest();
                expect(scope.tree[0].selected).toBeTruthy();
                expect(spy.calls.length).toBe(2);
                expect(spy.calls[0].args[0]).toBe('MK-TREE-EXPAND');
                expect(spy.calls[0].args[1]).toBe(initData[0]);
                expect(spy.calls[1].args[0]).toBe('MK-TREE-SELECT');
                expect(spy.calls[1].args[1]).toBe(initData[0]);
            }, {expandOnSelect: true, ajax: true})
        });

        it('multiple nodes', function(){
            initData[0].children.push({
                label: 'Node2',
                hasChildren: false,
                branch: {
                    label: 'Node2',
                    children: [],
                    hasChildren: false,
                    id: 'node-2'
                },
                children: [],
                collapsed: true,
                selected: false,
                editable: false,
                receivedChildren: false,
                icon: options.classes.iconLeaf,
                uuid: 'uuid-node-2',
                rootElement: null
            });
            runTest(html, initData, function (el) {
                var node1 = el.find('#uuid-node-1 > a'),
                    node2 = el.find('#uuid-node-2 > a'),
                    spy = spyOn(scope, '$emit'),
                    model1 = findModel(scope.tree, 'uuid-node-1'),
                    model2 = findModel(scope.tree, 'uuid-node-2');

                node1.trigger('click');
                node2.trigger('click');

                expect(model1.selected).toBeTruthy();
                expect(model2.selected).toBeTruthy();

                node2.trigger('click');
                expect(model2.selected).toBeFalsy();

            }, {multipleSelect: true})
        });
    });

    describe('adding node: ', function () {
        var suiteOptions = {editable: true, classes: {iconAdd: 'icon-add', iconCollapse: 'icon-collapse', editableBtnGroup: 'btn-group'}},
            response = {data: {data: {
                label: 'MY label',
                hasChildren: false,
                children: []
            }
            }};

        it('ajax is not allowed', function () {
            runTest(html, initData, function (el, timeout) {
                var node = el.find('#uuid-node-1'),
                    btn = el.find('.btn-group  i.icon-add'),
                    model = findModel(scope.tree, 'uuid-node-1');

                expect(node).not.toBeNull();
                expect(btn).not.toBeNull();
                spyOn(scope.$root, '$broadcast');
                btn.trigger('click');
                timeout(function(){
                    expect(scope.$root.$broadcast).not.toHaveBeenCalled();
                    expect(model.children.length).toBeGreaterThan(0);
                    expect(model.hasChildren).toBeTruthy();
                    expect(model.receivedChildren).toBeTruthy();
                    expect(model.collapsed).toBeFalsy();
                    expect(model.icon).toBe('icon-collapse');
                });

            }, angular.extend({ajax: false}, suiteOptions));
        });

        it('ajax is allowed, children have been received already', function () {

            runTest(html, initData, function (el) {
                var node = el.find('#uuid-root-node'),
                    btn = el.find('.btn-group  i.icon-add'),
                    model = findModel(scope.tree, 'uuid-root-node');

                expect(model.hasChildren).toBeTruthy();
                spyOn(scope.$root, '$broadcast').andCallThrough();

                btn.trigger('click');
                expect(scope.$root.$broadcast).toHaveBeenCalledWith('MK-TREE-ADD', model, jasmine.any(Object), jasmine.any(Function));

                //apply response to the callback
                scope.$root.$broadcast.calls[0].args[3](response);
                expect(model.hasChildren).toBeTruthy();
                expect(model.receivedChildren).toBeTruthy();
                expect(model.collapsed).toBeFalsy();
                expect(model.children.length).toBe(2);
                expect(model.children[1].label).toBe('New node');
                expect(model.children[1].branch.label).toBe('MY label');
            }, angular.extend({ajax: true}, suiteOptions));

        });

        it('ajax is allowed, children have NOT been received yet', function () {

            runTest(html, initData, function (el) {
                var node = el.find('#uuid-node-1'),
                    btn = node.parent().find('.btn-group  i.icon-add'),
                    model = findModel(scope.tree, 'uuid-node-1'),
                    spy = spyOn(scope.$root, '$broadcast').andCallThrough(),
                    response2 = {data: {data: [
                        {
                            label: 'MY child',
                            hasChildren: false,
                            children: []
                        }
                    ]
                    }};

                model.hasChildren = true;
//                //scope.$digest();

                expect(model.receivedChildren).toBeFalsy();

                btn.trigger('click');

                //Since we mocked call of expand method, event for adding hasn't been triggered
                expect(spy.calls.length).toBe(1);
                expect(spy.calls[0].args[0]).toBe('MK-TREE-EXPAND');
                expect(spy.calls[0].args[1]).toBe(model);

                //apply response to the callback
                spy.calls[0].args[2](response2);

//                //scope.$digest();

                expect(spy.calls.length).toBe(2);

                expect(spy.calls[1].args[0]).toBe('MK-TREE-ADD');
                expect(spy.calls[1].args[1]).toBe(model);

                //apply response to the callback
                spy.calls[1].args[3](response);

                expect(model.hasChildren).toBeTruthy();
                expect(model.receivedChildren).toBeTruthy();
                expect(model.collapsed).toBeFalsy();
                expect(model.children.length).toBe(2);
                expect(model.children[1].label).toBe('New node');
                expect(model.children[1].branch.label).toBe('MY label');
            }, angular.extend({ajax: true}, suiteOptions));

        });

    });

    describe('removing node:', function () {
        it('removing', function () {
            runTest(html, initData, function (el) {

                var node = el.find('#uuid-node-1'),
                    btn = node.parent().find('.btn-group  i.icon-remove'),
                    model = findModel(scope.tree, 'uuid-node-1'),
                    parent = scope.tree[0],
                    spy = spyOn(scope.$root, '$broadcast').andCallThrough();

                btn.trigger('click');

                expect(spy).toHaveBeenCalledWith('MK-TREE-REMOVE', model, jasmine.any(Function));

                //calling callback
                spy.calls[0].args[2]();

                expect(parent.children.length).toBe(0);
                expect(parent.hasChildren).toBeFalsy();
                expect(parent.icon).toBe('icon-leaf');
            }, {editable: true, classes: {iconRemove: 'icon-remove', editableBtnGroup: 'btn-group', iconLeaf: 'icon-leaf'}});
        });
    });

    describe('editing node', function () {
        it('editing', function () {

            runTest(html, initData, function (el, $timeout) {
                var node = el.find('#uuid-node-1'),
                    model = findModel(scope.tree, 'uuid-node-1'),
                    input,
                    event = new $.Event('keydown'),
                    rootNode = el.find('#uuid-root-node'),
                    rootModel = findModel(scope.tree, 'uuid-root-node'),
                    expandBtn;


                rootModel.icon = 'iconExpand';
//                //scope.$digest();
                expandBtn = rootNode.find('i.iconExpand');

                //first need to expand parent element
                expandBtn.trigger('click');

//                //scope.$digest();
                node.trigger('dblclick');

                expect(model.editable).toBeTruthy();
                input = node.find('input');
                expect(input).not.toBeNull();
                input.val('New label');
                event.keyCode = 13;
                input.trigger(event);
//                //scope.$digest();
                expect(model.editable).toBeFalsy();
            }, {editable: true, classes: {editableBtnGroup: 'btn-group'}});
        });
    });

    describe('moving node', function () {

        it('on the same parent', function () {
            runTest(html, initData, function (el) {
                var node = el.find('#uuid-node-1'),
                    parent = el.find('#uuid-root-node'),
                    dragEl = node.find('i.icon-drag'),
                    sc = angular.element(node).scope(),
                    spy = spyOn(scope.$root, '$broadcast').andCallThrough();

                sc.dragAndDrop(dragEl.scope(), parent.scope());
                expect(spy).not.toHaveBeenCalledWith('MK-TREE-DRAG-DROP');
            }, {editable: true, classes: {iconDraggable: 'icon-drag'}});
        });

        it('new parent', function () {
            initData[0].children[0].children.push({
                label: 'Node2',
                hasChildren: false,
                branch: {
                    label: 'Node2',
                    children: [],
                    hasChildren: false,
                    id: 'node-2'
                },
                children: [],
                collapsed: true,
                selected: false,
                editable: false,
                receivedChildren: false,
                icon: options.classes.iconLeaf,
                uuid: 'uuid-node-2',
                rootElement: null
            });
            initData[0].children[0].hasChildren = true;

            runTest(html, initData, function (el) {
                var node = el.find('#uuid-node-2'),
                    parent = el.find('#uuid-root-node'),
                    model = findModel(scope.tree, 'uuid-root-node'),
                    parentModel = findModel(scope.tree, 'uuid-node-1'),
                    dragEl = node.find('i.icon-drag'),
                    dropScope = angular.element(el.find('li')).scope(),
                    spy = spyOn(scope.$root, '$broadcast').andCallThrough();

                dropScope.dragAndDrop(dragEl.scope(), parent.scope());

                expect(spy).toHaveBeenCalled();
                expect(spy.calls.length).toBe(1);
                expect(spy.calls[0].args[0]).toBe('MK-TREE-DRAG-DROP');
                spy.calls[0].args[3]();
                //dropScope.$digest();

                expect(model.children.length).toBe(2);
                expect(parentModel.children.length).toBe(0);
            }, {editable: true, classes: {iconDraggable: 'icon-drag'}});
        });
    });
});
angular.module('mk.tree').directive('mkTree', ['mkBuilder', '$window', function (mkBuilder, $window) {
    'use strict';

    return {
        restrict: 'E',
        templateUrl: '/src/templates/tree.html',
        scope: {
            treeOptions: '=options',
            treeData: '=data',
            rootData: '=',
            selectedNodeId: '=selected',

            onExpand: '&',
            onSelect: '&',
            onRemove: '&',
            onAdd: '&',
            onEdit: '&',
            onDragDrop: '&',
            onConfirm: '&',
            onInit: '&'
        },

        controller: function ($scope, mkTreeConfig) {
            var active = false,
                onTreeDataChange = function (treeData) {
                    if (typeof treeData === 'undefined' || active) {
                        return;
                    }
                    //TODO: this flag is something that has to be refactored
                    active = true;

                    if ($scope.options.showRoot && angular.isObject($scope.rootData)) {
                        var ch = mkBuilder.buildTree(treeData, $scope.options, false);
                        $scope.rootData.hasChildren = $scope.rootData.hasChildren = ch.length > 0;
                        $scope.data = [mkBuilder.convertToNode($scope.rootData, ch, $scope.options, true)];
                    } else {
                        $scope.data = mkBuilder.buildTree(treeData, $scope.options, undefined);
                    }

                    var selectedNode;
                    if ($scope.selectedNodeId && (selectedNode = mkBuilder.findById($scope.data, $scope.selectedNodeId))){
                        angular.forEach(selectedNode.parents, function(node){
                            $scope.$broadcast('MK-TREE-DOWNEXPAND', node);
                        });
                        angular.forEach(selectedNode.node, function(node){
                            node.selected = true;
                            $scope.selectedNode = node;
                        });
                    }
                    $scope.onInit({'data': $scope.data});
                };
            $scope.loading = false;
            $scope.options = angular.extend(mkTreeConfig, $scope.treeOptions);
            $scope.$watch('treeData', onTreeDataChange, true);
        },

        link: function (scope, element, attrs) {

            scope.$on('MK-TREE-ADD-ROOT', function (event, data) {
                var item = mkBuilder.convertToNode(data, [], scope.options, true);
                item.editable = true;
                scope.data.push(item);
            });

            scope.$on('MK-TREE-EXPAND', function (event, node, callback) {
                if (attrs.onExpand) {
                    scope.loading = true;
                    scope.onExpand({'node': node.branch}).then(function (response) {
                        scope.loading = false;
                        if (callback) {
                            callback(response);
                        }
                    });
                }
            });

            scope.$on('MK-TREE-SELECT', function (event, node) {
                if (!scope.options.multipleSelect && scope.selectedNode && (scope.selectedNode !== node)) {
                    scope.selectedNode.selected = false;
                }
                scope.selectedNode = node;
                if (attrs.onSelect) {
                    var data = angular.extend(node.branch, {'selected':node.selected});
                    scope.onSelect({'node': data});
                }
            });

            scope.$on('MK-TREE-REMOVE', function (event, node, callback) {
                var okCallback = function () {
                        scope.onRemove({'node': node.branch}).then(function () {
                            callback();
                            scope.loading = false;
                        });
                    },
                    cancelCallback = function () {
                        scope.loading = false;
                    };

                if (scope.selectedNode === node) {
                    scope.selectedNode = undefined;
                }

                if (attrs.onRemove) {
                    scope.loading = true;

                    if (attrs.onConfirm) {
                        scope.onConfirm({'header': scope.options.translations.confirmHeader,
                            'message': scope.options.translations.confirmRemove, 'okCallback': okCallback, 'cancelCallback': cancelCallback});
                    } else {
                        if ($window.confirm(scope.options.translations.confirmRemove)) {
                            okCallback();
                        } else {
                            cancelCallback();
                        }
                    }
                } else {
                    callback();
                }
            });

            scope.$on('MK-TREE-ADD', function (event, node, newNode, callback) {
                if (attrs.onAdd) {
                    scope.loading = true;
                    scope.onAdd({'node': node.branch, 'data': newNode}).then(function (response) {
                        callback(response);
                        scope.loading = false;
                    });

                } else {
                    callback({'data': {'data': undefined}});
                }
            });

            scope.$on('MK-TREE-EDIT', function (event, node) {
                if (attrs.onEdit) {
                    scope.loading = true;
                    scope.onEdit({'node': node.branch}).then(function () {
                        scope.loading = false;
                    });
                }
            });

            scope.$on('MK-TREE-DRAG-DROP', function (event, source, target, callback) {
                if (attrs.onDragDrop) {
                    scope.loading = true;
                    scope.onDragDrop({'source': source.branch, 'target': target.branch}).then(function (response) {
                        callback(response);
                        scope.loading = false;
                    });
                } else {
                    callback();
                }
            });
        }
    };
}]);

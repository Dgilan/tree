angular.module('mk.tree').directive('mkDroppable', ['mkBuilder', 'mkTreeConfig', function (mkBuilder, mkTreeConfig) {
    'use strict';

    return {
        restrict: 'A',
        scope: {
            onDrop: '&'
        },
        link: function (scope, el) {
            var id = el.attr('id');

            if (!id) {
                id = mkBuilder.uuid();
                el.attr('id', id);
            }

            el.bind('dragover', function (e) {
                if (e.preventDefault) {
                    e.preventDefault(); // Necessary. Allows us to drop.
                }

                e.originalEvent.dataTransfer.dropEffect = 'move';
                return false;
            });

            el.bind('dragenter', function () {
                el.addClass(mkTreeConfig.classes.over);
                scope.$root.$emit('MK-DRAG-ENTER', id);
            });

            el.bind('drop', function (e) {
                var data;
                if (e.preventDefault) {
                    e.preventDefault(); // Necessary. Allows us to drop.
                }
                if (e.stopPropogation) {
                    e.stopPropogation(); // Necessary. Allows us to drop.
                }

                data = e.originalEvent.dataTransfer.getData('text');
                scope.onDrop({dragEl: angular.element(document.getElementById(data)).scope(), dropEl: angular.element(el.context).scope()});
                scope.$root.$emit('MK-DRAG-END');
            });

            /**
             * Listens dragenter event and remove over class if other target element is onHover
             */
            scope.$root.$on('MK-DRAG-ENTER', function (e, _id) {
                if (id !== _id) {
                    el.removeClass(mkTreeConfig.classes.over);
                }
            });

            /**
             * Adding target class to all droppable elements
             */
            scope.$root.$on('MK-DRAG-START', function () {
                el.addClass(mkTreeConfig.classes.target);
            });

            /**
             * Removing target and over classes from all droppable elements
             */
            scope.$root.$on('MK-DRAG-END', function () {
                el.removeClass(mkTreeConfig.classes.target);
                el.removeClass(mkTreeConfig.classes.over);
            });
        }
    };
}]);

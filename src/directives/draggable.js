angular.module('mk.tree').directive('mkDraggable', ['mkBuilder', function (mkBuilder) {
    'use strict';

    return {
        restrict: 'A',
        link: function (scope, el) {
            var id = el.attr('id');
            el.attr('draggable', 'true');

            if (!id) {
                id = mkBuilder.uuid();
                el.attr('id', id);
            }

            el.bind('dragstart', function (e) {
                e.originalEvent.dataTransfer.setData('text', id);
                scope.$root.$emit('MK-DRAG-START');
            });

            el.bind('dragend', function () {
                scope.$root.$emit('MK-DRAG-END');
            });
        }
    };
}
]);

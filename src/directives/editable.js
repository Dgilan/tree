angular.module('mk.tree').directive('mkTreeEdit', ['$window', '$timeout', 'mkTreeConfig', function ($window, $timeout, mkTreeConfig) {
    'use strict';

    (function (jQuery) {
        jQuery.fn.clickoutside = jQuery.fn.clickoutside || function (callback) {
            var outside = 1, self = jQuery(this);

            self.cb = callback;

            this.click(function () {
                outside = 0;
            });

            jQuery(document).click(function () {
                if (outside) {
                    self.cb();
                }
                outside = 1;
            });
            return self;
        };
    })(window.jQuery);

    return {
        restrict: 'E',
        scope: {
            model: '=',
            onSave: '&',
            onSelect: '&'
        },
        priority: 10,
        templateUrl: '/src/templates/tree.edit.html',
        link: function (scope, element) {
            var scopeCache = scope.model.label, changingMode = false;

            scope.$watch('model.editable', function (val) {
                if (val) {
                    //Changing mode flag is set in order to clickouside event don't call save function
                    changingMode = true;

                    $timeout(function () {
                        element.find('input:first').select().focus();
                        changingMode = false;
                    });
                }
            });

            scope.classes = mkTreeConfig.classes;

            /**
             * Saves branch label
             */
            function save() {
                if (typeof scope.onSave === 'function') {
                    scope.$apply(function (sc) {
                        if (scopeCache === sc.model.label) {
                            return;
                        }

                        sc.onSave({'node': sc.model});
                    });
                }
                scopeCache = scope.model.label;
                scope.$apply('model.editable=false');
            }

            /**
             * Reverts all changes
             */
            function revert() {
                if (!scope.model.editable) {
                    return;
                }

                scope.$apply(function (sc) {
                    sc.model.editable = false;
                    sc.model.label = scopeCache;
                });
            }

            //Saving on click outside event
            element.clickoutside(function () {
                if (scope.model && scope.model.editable && !changingMode) {
                    save();
                }
            });

            // Binds events to all input inside the edited element
            element.find('input:first').bind('keydown', function (event) {
                var esc = event.keyCode === 27,
                    enter = event.keyCode === 13;

                if (esc) {
                    // The event is calling because of events priority:
                    // the model should be changed before applying cache
                    this.blur();
                    //event.stopPropagation();
                } else if (enter) {
                    save();
                }
            });

            //Binds event to the global window in order to hitting "escape" will cause reverting all changes
            angular.element($window).bind('keydown', function (event) {
                if (event.keyCode === 27) {
                    revert();
                }
            });

            scope.select = function (node) {
                scope.onSelect({'node': node});
            };
        }
    };
}]);

angular.module('mk.tree').directive('mkTreeChildren', ['mkBuilder', '$timeout', function (mkBuilder, $timeout) {
    'use strict';

    return {
        restrict: 'E',
        templateUrl: '/src/templates/tree.children.html',
        scope: {
            data: '=',
            options: '='
        },
        compile: function (element) {
            return mkBuilder.compile(element, function (scope) {
                var toggleNode = function (node) {
                        node.collapsed = !node.collapsed;
                        node.icon = node.collapsed ? scope.options.classes.iconExpand : scope.options.classes.iconCollapse;
                    },
                    expandNode = function (node) {
                        node.collapsed = false;
                        node.icon = scope.options.classes.iconCollapse;
                    };

                //Sort children
                scope.sort = function (node) {
                    if (typeof scope.options.sort !== 'undefined' && node.branch.hasOwnProperty(scope.options.sort)) {
                        return node.branch[scope.options.sort];
                    }
                };

                //Toggle
                scope.toggle = function (node) {
                    if (node.hasChildren) {
                        /// Supporting ajax loading, children are not received and not predefined
                        if (scope.options.ajax && !node.receivedChildren && !node.children.length) {
                            scope.$parent.$emit('MK-TREE-EXPAND', node, function (response) {
                                var children = [];
                                angular.forEach(response.data.data, function (item) {
                                    children.push(mkBuilder.convertToNode(item, [], scope.options));
                                });
                                node.children = children;
                                node.receivedChildren = true;
                            });
                        }
                        toggleNode(node);
                    }
                };

                //Select node
                scope.select = function (node) {
                    if (scope.options.selectOnlyChildren && node.hasChildren) {
                        scope.toggle(node);
                    } else {
                        if (scope.options.expandOnSelect) {
                            scope.toggle(node);
                        }
                        node.selected = scope.options.multipleSelect ? !node.selected : true;
                        scope.$parent.$emit('MK-TREE-SELECT', node);
                    }
                };

                //Remove node
                scope.remove = function (node) {
                    var self = this, parent = self.$parent.$parent.$parent;
                    scope.$root.$broadcast('MK-TREE-REMOVE', node, function () {
                        if (parent.node !== undefined && (!self.$parent.data || !self.$parent.data.length || self.$parent.data.length === 1 )) {
                            parent.node.hasChildren = false;
                            parent.node.icon = scope.options.classes.iconLeaf;
                        }
                        self.$parent.data.splice(self.$index, 1);
                    });
                };

                //Add node
                scope.add = function (node) {
                    var item = mkBuilder.newNode(scope.options),
                        addItem = function (node, item, data) {
                            item.branch = data;
                            node.children.push(item);
                            node.hasChildren = true;
                            node.receivedChildren = true;
                            expandNode(node);
                        },
                        getAndAddItem = function (node) {
                            scope.$root.$broadcast('MK-TREE-ADD', node, item, function (response) {
                                addItem(node, item, response.data.data);
                            });
                        };

                    if (scope.options.ajax) {

                        if (node.hasChildren && !node.receivedChildren && !node.children.length) {
                            //node has children, but we haven't received them yet
                            scope.$root.$broadcast('MK-TREE-EXPAND', node, function (response) {
                                var children = [];
                                angular.forEach(response.data.data, function (item) {
                                    children.push(mkBuilder.convertToNode(angular.copy(item), [], scope.options));
                                });
                                node.children = children;
                                getAndAddItem(node);
                            });
                        } else {
                            getAndAddItem(node);
                        }

                    } else {
                        $timeout(function () {
                            addItem(node, item, {});
                        });
                    }
                };

                //Edit node
                scope.edit = function (node) {
                    node.branch.label = node.label;
                    scope.$root.$broadcast('MK-TREE-EDIT', node);
                };

                //Drag and drop
                scope.dragAndDrop = function (dragScope, dropScope) {
                    var isNewParent = function (drag, drop) {
                            var isNew = true;
                            if (drop.hasChildren) {
                                angular.forEach(drop.children, function (child) {
                                    if (child.uuid === drag.uuid) {
                                        isNew = false;
                                    }
                                });
                            }
                            return isNew;
                        },
                        dragBranch = dragScope.node,
                        dropBranch = dropScope.node,
                        dragParent = dragScope.$parent.$parent.$parent;

                    if (dragBranch !== dropBranch) {
                        if (isNewParent(dragBranch, dropBranch) && isNewParent(dropBranch, dragBranch)) {
                            scope.$root.$broadcast('MK-TREE-DRAG-DROP', dragBranch, dropBranch, function () {
                                var copy = angular.copy(dragBranch);
                                if (dragScope.data.length === 1) {
                                    dragParent.node.hasChildren = false;
                                    dragParent.node.icon = scope.options.iconLeaf;
                                }
                                dragScope.data.splice(dragScope.$index, 1);

                                if (scope.options.ajax && dropBranch.hasChildren && !dropBranch.receivedChildren) {
                                    scope.$emit('MK-TREE-EXPAND', dropBranch, function (response) {
                                        var children = [];
                                        angular.forEach(response.data.data, function (item) {
                                            children.push(mkBuilder.convertToNode(item, [], scope.options));
                                        });
                                        dropBranch.children = children;
                                        dropBranch.receivedChildren = true;
                                        expandNode(dropBranch);
                                    });
                                } else {
                                    dropBranch.receivedChildren = true;
                                    dropBranch.hasChildren = true;
                                    dropBranch.children.push(copy);
                                    dropBranch.icon = dropBranch.collapsed ? scope.options.iconExpand : scope.options.iconCollapse;
                                    expandNode(dropBranch);
                                }

//                                    scope.$digest();
                            });
                        }
                    }
                };

                //Expand node event handler
                scope.$on('MK-TREE-DOWNEXPAND',function(event, node){
                    expandNode(node);
                    event.preventDefault();
                });
            });
        }
    };
}]);

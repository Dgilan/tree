angular.module('mk.tree', []).constant('mkTreeConfig', {
    classes: {
        over: 'mk-droppable-over',
        target: 'mk-droppable-target',
        intended: 'mk-intended',
        iconLeaf: 'glyphicon glyphicon-unchecked',
        iconExpand: 'glyphicon glyphicon-chevron-right',
        iconCollapse: 'glyphicon glyphicon-chevron-down',
        iconAdd: 'glyphicon glyphicon-plus-sign',
        iconRemove: 'glyphicon glyphicon-remove-sign',
        iconDraggable: 'glyphicon glyphicon-move',
        editableBtnGroup: 'mk-btn-group',
        selected: 'mk-selected',
        li: 'mk-tree-row',
        draggable: 'mk-tree-draggable'
    },
    translations: {
        confirmRemove: 'Are you sure you want to remove the item?'
    },
    ajax: false,
    editable: false,
    showRoot: false,
    selectOnlyChildren: false,
    expandOnSelect: true,
    multipleSelect: false
});

angular.module('mk.tree').directive('mkTreeChildren', ['mkBuilder', '$timeout', function (mkBuilder, $timeout) {
    'use strict';

    return {
        restrict: 'E',
        templateUrl: '/src/templates/tree.children.html',
        scope: {
            data: '=',
            options: '='
        },
        compile: function (element) {
            return mkBuilder.compile(element, function (scope) {
                var toggleNode = function (node) {
                        node.collapsed = !node.collapsed;
                        node.icon = node.collapsed ? scope.options.classes.iconExpand : scope.options.classes.iconCollapse;
                    },
                    expandNode = function (node) {
                        node.collapsed = false;
                        node.icon = scope.options.classes.iconCollapse;
                    };

                //Sort children
                scope.sort = function (node) {
                    if (typeof scope.options.sort !== 'undefined' && node.branch.hasOwnProperty(scope.options.sort)) {
                        return node.branch[scope.options.sort];
                    }
                };

                //Toggle
                scope.toggle = function (node) {
                    if (node.hasChildren) {
                        /// Supporting ajax loading, children are not received and not predefined
                        if (scope.options.ajax && !node.receivedChildren && !node.children.length) {
                            scope.$parent.$emit('MK-TREE-EXPAND', node, function (response) {
                                var children = [];
                                angular.forEach(response.data.data, function (item) {
                                    children.push(mkBuilder.convertToNode(item, [], scope.options));
                                });
                                node.children = children;
                                node.receivedChildren = true;
                            });
                        }
                        toggleNode(node);
                    }
                };

                //Select node
                scope.select = function (node) {
                    if (scope.options.selectOnlyChildren && node.hasChildren) {
                        scope.toggle(node);
                    } else {
                        if (scope.options.expandOnSelect) {
                            scope.toggle(node);
                        }
                        node.selected = scope.options.multipleSelect ? !node.selected : true;
                        scope.$parent.$emit('MK-TREE-SELECT', node);
                    }
                };

                //Remove node
                scope.remove = function (node) {
                    var self = this, parent = self.$parent.$parent.$parent;
                    scope.$root.$broadcast('MK-TREE-REMOVE', node, function () {
                        if (parent.node !== undefined && (!self.$parent.data || !self.$parent.data.length || self.$parent.data.length === 1 )) {
                            parent.node.hasChildren = false;
                            parent.node.icon = scope.options.classes.iconLeaf;
                        }
                        self.$parent.data.splice(self.$index, 1);
                    });
                };

                //Add node
                scope.add = function (node) {
                    var item = mkBuilder.newNode(scope.options),
                        addItem = function (node, item, data) {
                            item.branch = data;
                            node.children.push(item);
                            node.hasChildren = true;
                            node.receivedChildren = true;
                            expandNode(node);
                        },
                        getAndAddItem = function (node) {
                            scope.$root.$broadcast('MK-TREE-ADD', node, item, function (response) {
                                addItem(node, item, response.data.data);
                            });
                        };

                    if (scope.options.ajax) {

                        if (node.hasChildren && !node.receivedChildren && !node.children.length) {
                            //node has children, but we haven't received them yet
                            scope.$root.$broadcast('MK-TREE-EXPAND', node, function (response) {
                                var children = [];
                                angular.forEach(response.data.data, function (item) {
                                    children.push(mkBuilder.convertToNode(angular.copy(item), [], scope.options));
                                });
                                node.children = children;
                                getAndAddItem(node);
                            });
                        } else {
                            getAndAddItem(node);
                        }

                    } else {
                        $timeout(function () {
                            addItem(node, item, {});
                        });
                    }
                };

                //Edit node
                scope.edit = function (node) {
                    node.branch.label = node.label;
                    scope.$root.$broadcast('MK-TREE-EDIT', node);
                };

                //Drag and drop
                scope.dragAndDrop = function (dragScope, dropScope) {
                    var isNewParent = function (drag, drop) {
                            var isNew = true;
                            if (drop.hasChildren) {
                                angular.forEach(drop.children, function (child) {
                                    if (child.uuid === drag.uuid) {
                                        isNew = false;
                                    }
                                });
                            }
                            return isNew;
                        },
                        dragBranch = dragScope.node,
                        dropBranch = dropScope.node,
                        dragParent = dragScope.$parent.$parent.$parent;

                    if (dragBranch !== dropBranch) {
                        if (isNewParent(dragBranch, dropBranch) && isNewParent(dropBranch, dragBranch)) {
                            scope.$root.$broadcast('MK-TREE-DRAG-DROP', dragBranch, dropBranch, function () {
                                var copy = angular.copy(dragBranch);
                                if (dragScope.data.length === 1) {
                                    dragParent.node.hasChildren = false;
                                    dragParent.node.icon = scope.options.iconLeaf;
                                }
                                dragScope.data.splice(dragScope.$index, 1);

                                if (scope.options.ajax && dropBranch.hasChildren && !dropBranch.receivedChildren) {
                                    scope.$emit('MK-TREE-EXPAND', dropBranch, function (response) {
                                        var children = [];
                                        angular.forEach(response.data.data, function (item) {
                                            children.push(mkBuilder.convertToNode(item, [], scope.options));
                                        });
                                        dropBranch.children = children;
                                        dropBranch.receivedChildren = true;
                                        expandNode(dropBranch);
                                    });
                                } else {
                                    dropBranch.receivedChildren = true;
                                    dropBranch.hasChildren = true;
                                    dropBranch.children.push(copy);
                                    dropBranch.icon = dropBranch.collapsed ? scope.options.iconExpand : scope.options.iconCollapse;
                                    expandNode(dropBranch);
                                }

//                                    scope.$digest();
                            });
                        }
                    }
                };

                //Expand node event handler
                scope.$on('MK-TREE-DOWNEXPAND',function(event, node){
                    expandNode(node);
                    event.preventDefault();
                });
            });
        }
    };
}]);

angular.module('mk.tree').directive('mkDraggable', ['mkBuilder', function (mkBuilder) {
    'use strict';

    return {
        restrict: 'A',
        link: function (scope, el) {
            var id = el.attr('id');
            el.attr('draggable', 'true');

            if (!id) {
                id = mkBuilder.uuid();
                el.attr('id', id);
            }

            el.bind('dragstart', function (e) {
                e.originalEvent.dataTransfer.setData('text', id);
                scope.$root.$emit('MK-DRAG-START');
            });

            el.bind('dragend', function () {
                scope.$root.$emit('MK-DRAG-END');
            });
        }
    };
}
]);

angular.module('mk.tree').directive('mkDroppable', ['mkBuilder', 'mkTreeConfig', function (mkBuilder, mkTreeConfig) {
    'use strict';

    return {
        restrict: 'A',
        scope: {
            onDrop: '&'
        },
        link: function (scope, el) {
            var id = el.attr('id');

            if (!id) {
                id = mkBuilder.uuid();
                el.attr('id', id);
            }

            el.bind('dragover', function (e) {
                if (e.preventDefault) {
                    e.preventDefault(); // Necessary. Allows us to drop.
                }

                e.originalEvent.dataTransfer.dropEffect = 'move';
                return false;
            });

            el.bind('dragenter', function () {
                el.addClass(mkTreeConfig.classes.over);
                scope.$root.$emit('MK-DRAG-ENTER', id);
            });

            el.bind('drop', function (e) {
                var data;
                if (e.preventDefault) {
                    e.preventDefault(); // Necessary. Allows us to drop.
                }
                if (e.stopPropogation) {
                    e.stopPropogation(); // Necessary. Allows us to drop.
                }

                data = e.originalEvent.dataTransfer.getData('text');
                scope.onDrop({dragEl: angular.element(document.getElementById(data)).scope(), dropEl: angular.element(el.context).scope()});
                scope.$root.$emit('MK-DRAG-END');
            });

            /**
             * Listens dragenter event and remove over class if other target element is onHover
             */
            scope.$root.$on('MK-DRAG-ENTER', function (e, _id) {
                if (id !== _id) {
                    el.removeClass(mkTreeConfig.classes.over);
                }
            });

            /**
             * Adding target class to all droppable elements
             */
            scope.$root.$on('MK-DRAG-START', function () {
                el.addClass(mkTreeConfig.classes.target);
            });

            /**
             * Removing target and over classes from all droppable elements
             */
            scope.$root.$on('MK-DRAG-END', function () {
                el.removeClass(mkTreeConfig.classes.target);
                el.removeClass(mkTreeConfig.classes.over);
            });
        }
    };
}]);

angular.module('mk.tree').directive('mkTreeEdit', ['$window', '$timeout', 'mkTreeConfig', function ($window, $timeout, mkTreeConfig) {
    'use strict';

    (function (jQuery) {
        jQuery.fn.clickoutside = jQuery.fn.clickoutside || function (callback) {
            var outside = 1, self = jQuery(this);

            self.cb = callback;

            this.click(function () {
                outside = 0;
            });

            jQuery(document).click(function () {
                if (outside) {
                    self.cb();
                }
                outside = 1;
            });
            return self;
        };
    })(window.jQuery);

    return {
        restrict: 'E',
        scope: {
            model: '=',
            onSave: '&',
            onSelect: '&'
        },
        priority: 10,
        templateUrl: '/src/templates/tree.edit.html',
        link: function (scope, element) {
            var scopeCache = scope.model.label, changingMode = false;

            scope.$watch('model.editable', function (val) {
                if (val) {
                    //Changing mode flag is set in order to clickouside event don't call save function
                    changingMode = true;

                    $timeout(function () {
                        element.find('input:first').select().focus();
                        changingMode = false;
                    });
                }
            });

            scope.classes = mkTreeConfig.classes;

            /**
             * Saves branch label
             */
            function save() {
                if (typeof scope.onSave === 'function') {
                    scope.$apply(function (sc) {
                        if (scopeCache === sc.model.label) {
                            return;
                        }

                        sc.onSave({'node': sc.model});
                    });
                }
                scopeCache = scope.model.label;
                scope.$apply('model.editable=false');
            }

            /**
             * Reverts all changes
             */
            function revert() {
                if (!scope.model.editable) {
                    return;
                }

                scope.$apply(function (sc) {
                    sc.model.editable = false;
                    sc.model.label = scopeCache;
                });
            }

            //Saving on click outside event
            element.clickoutside(function () {
                if (scope.model && scope.model.editable && !changingMode) {
                    save();
                }
            });

            // Binds events to all input inside the edited element
            element.find('input:first').bind('keydown', function (event) {
                var esc = event.keyCode === 27,
                    enter = event.keyCode === 13;

                if (esc) {
                    // The event is calling because of events priority:
                    // the model should be changed before applying cache
                    this.blur();
                    //event.stopPropagation();
                } else if (enter) {
                    save();
                }
            });

            //Binds event to the global window in order to hitting "escape" will cause reverting all changes
            angular.element($window).bind('keydown', function (event) {
                if (event.keyCode === 27) {
                    revert();
                }
            });

            scope.select = function (node) {
                scope.onSelect({'node': node});
            };
        }
    };
}]);

angular.module('mk.tree').directive('mkTree', ['mkBuilder', '$window', function (mkBuilder, $window) {
    'use strict';

    return {
        restrict: 'E',
        templateUrl: '/src/templates/tree.html',
        scope: {
            treeOptions: '=options',
            treeData: '=data',
            rootData: '=',
            selectedNodeId: '=selected',

            onExpand: '&',
            onSelect: '&',
            onRemove: '&',
            onAdd: '&',
            onEdit: '&',
            onDragDrop: '&',
            onConfirm: '&',
            onInit: '&'
        },

        controller: function ($scope, mkTreeConfig) {
            var active = false,
                onTreeDataChange = function (treeData) {
                    if (typeof treeData === 'undefined' || active) {
                        return;
                    }
                    //TODO: this flag is something that has to be refactored
                    active = true;

                    if ($scope.options.showRoot && angular.isObject($scope.rootData)) {
                        var ch = mkBuilder.buildTree(treeData, $scope.options, false);
                        $scope.rootData.hasChildren = $scope.rootData.hasChildren = ch.length > 0;
                        $scope.data = [mkBuilder.convertToNode($scope.rootData, ch, $scope.options, true)];
                    } else {
                        $scope.data = mkBuilder.buildTree(treeData, $scope.options, undefined);
                    }

                    var selectedNode;
                    if ($scope.selectedNodeId && (selectedNode = mkBuilder.findById($scope.data, $scope.selectedNodeId))){
                        angular.forEach(selectedNode.parents, function(node){
                            $scope.$broadcast('MK-TREE-DOWNEXPAND', node);
                        });
                        angular.forEach(selectedNode.node, function(node){
                            node.selected = true;
                            $scope.selectedNode = node;
                        });
                    }
                    $scope.onInit({'data': $scope.data});
                };
            $scope.loading = false;
            $scope.options = angular.extend(mkTreeConfig, $scope.treeOptions);
            $scope.$watch('treeData', onTreeDataChange, true);
        },

        link: function (scope, element, attrs) {

            scope.$on('MK-TREE-ADD-ROOT', function (event, data) {
                var item = mkBuilder.convertToNode(data, [], scope.options, true);
                item.editable = true;
                scope.data.push(item);
            });

            scope.$on('MK-TREE-EXPAND', function (event, node, callback) {
                if (attrs.onExpand) {
                    scope.loading = true;
                    scope.onExpand({'node': node.branch}).then(function (response) {
                        scope.loading = false;
                        if (callback) {
                            callback(response);
                        }
                    });
                }
            });

            scope.$on('MK-TREE-SELECT', function (event, node) {
                if (!scope.options.multipleSelect && scope.selectedNode && (scope.selectedNode !== node)) {
                    scope.selectedNode.selected = false;
                }
                scope.selectedNode = node;
                if (attrs.onSelect) {
                    var data = angular.extend(node.branch, {'selected':node.selected});
                    scope.onSelect({'node': data});
                }
            });

            scope.$on('MK-TREE-REMOVE', function (event, node, callback) {
                var okCallback = function () {
                        scope.onRemove({'node': node.branch}).then(function () {
                            callback();
                            scope.loading = false;
                        });
                    },
                    cancelCallback = function () {
                        scope.loading = false;
                    };

                if (scope.selectedNode === node) {
                    scope.selectedNode = undefined;
                }

                if (attrs.onRemove) {
                    scope.loading = true;

                    if (attrs.onConfirm) {
                        scope.onConfirm({'header': scope.options.translations.confirmHeader,
                            'message': scope.options.translations.confirmRemove, 'okCallback': okCallback, 'cancelCallback': cancelCallback});
                    } else {
                        if ($window.confirm(scope.options.translations.confirmRemove)) {
                            okCallback();
                        } else {
                            cancelCallback();
                        }
                    }
                } else {
                    callback();
                }
            });

            scope.$on('MK-TREE-ADD', function (event, node, newNode, callback) {
                if (attrs.onAdd) {
                    scope.loading = true;
                    scope.onAdd({'node': node.branch, 'data': newNode}).then(function (response) {
                        callback(response);
                        scope.loading = false;
                    });

                } else {
                    callback({'data': {'data': undefined}});
                }
            });

            scope.$on('MK-TREE-EDIT', function (event, node) {
                if (attrs.onEdit) {
                    scope.loading = true;
                    scope.onEdit({'node': node.branch}).then(function () {
                        scope.loading = false;
                    });
                }
            });

            scope.$on('MK-TREE-DRAG-DROP', function (event, source, target, callback) {
                if (attrs.onDragDrop) {
                    scope.loading = true;
                    scope.onDragDrop({'source': source.branch, 'target': target.branch}).then(function (response) {
                        callback(response);
                        scope.loading = false;
                    });
                } else {
                    callback();
                }
            });
        }
    };
}]);

angular.module('mk.tree').factory('mkBuilder', ['$compile', function ($compile) {
    'use strict';

    var uuid = function () {
            function _p8(s) {
                var p = (Math.random().toString(16) + '000000000').substr(2, 8);
                return s ? '-' + p.substr(0, 4) + '-' + p.substr(4, 4) : p;
            }

            return _p8() + _p8(true) + _p8(true) + _p8();
        },
        convertToNode = function (data, children, options, root) {
            //TODO: set data.label as dynamic key
            var hasChildren = typeof data.hasChildren === 'undefined' ? children.length > 0 : data.hasChildren;
            return {
                label: data.label ? data.label : 'No label',
                hasChildren: hasChildren,
                branch: data,
                children: children,
                collapsed: true,
                selected: false,
                editable: false,
                receivedChildren: hasChildren && options.ajax,
                icon: hasChildren ? options.classes.iconExpand : options.classes.iconLeaf,
                uuid: uuid(),
                rootElement: root
            };
        },
        buildTree = function (treeData, options, setAsRoot) {
            var tree = [], isRoot = typeof setAsRoot !== 'undefined' ? setAsRoot : true;

            function doBuild(node) {
                var children = [];
                angular.forEach(node.children, function (child) {
                    children.push(doBuild(child));
                });
                return convertToNode(node, children, options);
            }

            angular.forEach(treeData, function (node) {
                var n = doBuild(node);
                if (isRoot) {
                    n.rootElement = true;
                }
                tree.push(n);
            });
            return tree;
        };

    return {
        uuid: uuid,

        /**
         * Manually compiles the element, fixing the recursion loop.
         * @param {Node} element
         * @param {Function|Object} [link] A post-link function, or an object with function(s) registered via pre and post properties.
         * @returns {Object} An object containing the linking functions.
         */
        compile: function (element, link) {
            // Normalize the link parameter
            if (angular.isFunction(link)) {
                link = { post: link };
            }

            // Break the recursion loop by removing the contents
            var contents = element.contents().remove(),
                compiledContents;

            return {
                pre: (link && link.pre) ? link.pre : null,
                /**
                 * Compiles and re-adds the contents
                 */
                post: function (scope, element) {
                    // Compile the contents
                    if (!compiledContents) {
                        compiledContents = $compile(contents);
                    }
                    // Re-add the compiled contents to the element
                    compiledContents(scope, function (clone) {
                        element.append(clone);
                    });

                    // Call the post-linking function, if any
                    if (link && link.post) {
                        link.post.apply(null, arguments);
                    }
                }
            };
        },
        convertToNode: convertToNode,
        buildTree: buildTree,
        newNode: function (options) {
            return {
                label: 'New node',
                hasChildren: false,
                branch: undefined,
                children: [],
                collapsed: false,
                selected: false,
                editable: true,
                receivedChildren: false,
                icon: options.classes.iconLeaf,
                uuid: uuid(),
                rootElement: null
            };
        },
        findById: function (tree, id) {
            var doFind = function (node, parents) {
                var res;
                parents.push(node);
                angular.forEach(node.children, function (child) {
                    res = res || ((id.indexOf(child.branch.id) > -1) ? {
                        node: child,
                        parents: parents
                    } : doFind(child, parents));
                });
                return res;
            }, result = {'node': [], 'parents': []}, data;

            if(!angular.isArray(id)){
                id = [id];
            }

            angular.forEach(tree, function (node) {
                data = ((id.indexOf(node.branch.id) > -1) ? {'node': node, 'parents': []} : doFind(node, []));
                if(data) {
                    result.node.push(data.node);
                    result.parents = result.parents.concat(data.parents);
                }
            });
            return result;
        }
    };
}]);

angular.module('mk.tree').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('/src/templates/tree.children.html',
    "<li ng-repeat=\"node in data |orderBy:sort track by node.uuid\" ng-animate=mk-tree-animate class={{options.classes.li}} ng-class=\"node.selected? options.classes.selected: ''\" role=treeitem><span ng-if=!options.editable id={{node.uuid}} title=\"{{node.hasChildren?(node.collapsed?'Expand this branch':'Collapse this branch'):''}}\"><i ng-class=node.icon ng-click=toggle(node)></i> <a ng-click=select(node)>{{node.label}}</a></span> <span ng-if=options.editable id={{node.uuid}} mk-droppable=true title=\"{{node.hasChildren?(node.collapsed?'Expand this branch':'Collapse this branch'):''}}\" ng-dblclick=\"node.editable = true\" on-drop=\"dragAndDrop(dragEl, dropEl)\"><i class={{options.classes.draggable}} mk-draggable=true ng-class=options.classes.iconDraggable></i> <i ng-class=node.icon ng-click=toggle(node)></i><mk-tree-edit model=node on-save=edit(node) on-select=select(node)></mk-tree-edit></span><section ng-if=\"options && options.editable\" ng-hide=node.editable ng-class=options.classes.editableBtnGroup><i ng-class=options.classes.iconAdd title=\"Add child\" ng-click=add(node)></i> <i ng-class=options.classes.iconRemove title=Remove ng-click=remove(node)></i></section><ul ng-if=node.hasChildren ng-hide=node.collapsed role=group><mk-tree-children data=node.children options=options></mk-tree-children></ul></li>"
  );


  $templateCache.put('/src/templates/tree.edit.html',
    "<a class={{classes.intended}} ng-click=select(model) ng-hide=model.editable>{{model.label}}</a> <input class={{classes.intended}} ng-model=model.label ng-show=model.editable>"
  );


  $templateCache.put('/src/templates/tree.html',
    "<ul role=tree class=mk-tree><div class=mk-loader-mask ng-show=loading></div><div class=mk-tree-data><mk-tree-children data=data options=options></mk-tree-children></div></ul>"
  );

}]);
